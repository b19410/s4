--a. Find all artists that has letter d in its name.
	SELECT * FROM artists WHERE name LIKE "%d%";

--b. Find all songs that has a length less than 230.
	SELECT * FROM songs WHERE length < 230;

--c. Join the "albums" and "songs" table. (Only show tha album name, song name and song length.)
	SELECT album_title, song_name, length FROM artist
		JOIN albums ON artist.id = albums.artist_id
		JOIN songs ON albums.id = songs.album_id;

--d. Join the "artists" and "albums" table. (Find all albums that has letter a in ints name)
	SELECT * FROM artist 
		INNER JOIN albums ON artist.id = albums.artist_id WHERE name LIKE "%a%";

--e. Sort albums from A-Z order. (Show only the first 4 records)
	SELECT * FROM albums ORDER BY album_title ASC LIMIT 4;

--f. Join the "albums" and "songs" tables. (Sort albums from Z-A and sort songs from A-Z)
	SELECT album_title, song_name FROM artist
		JOIN albums ON artist.id = albums.artist_id ORDER BY album_title DESC
		JOIN songs ON albums.id = songs.album_id ORDER BY song_name ASC; 


